package com.juntadeandalucia.ced.domain.login

import com.juntadeandalucia.ced.commons.data.types.Either

interface LoginRepository {
    suspend fun checkLogin(loginInput: LoginInput): Either<CheckLoginError, Unit>
}