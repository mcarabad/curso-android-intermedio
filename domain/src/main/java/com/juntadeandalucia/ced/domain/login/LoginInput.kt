package com.juntadeandalucia.ced.domain.login

data class LoginInput(val username: String, val password: String, val version: String)