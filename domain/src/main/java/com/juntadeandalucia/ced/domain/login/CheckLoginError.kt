package com.juntadeandalucia.ced.domain.login

import com.juntadeandalucia.ced.domain.RepositoryFailure

sealed class CheckLoginError {
    data class Failure (val error: RepositoryFailure): CheckLoginError()
    data class Known(val error: LoginError): CheckLoginError()
}