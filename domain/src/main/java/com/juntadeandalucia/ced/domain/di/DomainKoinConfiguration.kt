package com.juntadeandalucia.ced.domain.di

import com.juntadeandalucia.ced.domain.login.CheckLogin
import org.koin.dsl.module

class DomainKoinConfiguration {

    fun getModule() = module{
        //User case
        factory { CheckLogin(get()) }
    }
}