package com.juntadeandalucia.ced.data.login

import com.juntadeandalucia.ced.commons.data.types.Either
import com.juntadeandalucia.ced.data.remote.NoContentResponse
import com.juntadeandalucia.ced.data.remote.ParsedResponse
import com.juntadeandalucia.ced.data.remote.RemoteDataSourceExecutor
import com.juntadeandalucia.ced.domain.login.CheckLoginError
import com.juntadeandalucia.ced.domain.login.LoginError
import com.juntadeandalucia.ced.domain.login.LoginInput
import com.juntadeandalucia.ced.domain.login.LoginRepository

class LoginRepositoryImpl(
    private val loginRemoteDataSource: LoginRemoteDataSource,
    private val remoteDataSourceExecutor: RemoteDataSourceExecutor
) : LoginRepository {
    override suspend fun checkLogin(loginInput: LoginInput): Either<CheckLoginError, Unit> {
        val loginRequest =
            LoginRequest(loginInput.username, loginInput.password, loginInput.version)

        val parsedResponse: ParsedResponse<LoginError, NoContentResponse> =
            remoteDataSourceExecutor { loginRemoteDataSource.checkLogin(loginRequest) }

        return when(parsedResponse) {
            is ParsedResponse.Success -> Either.Right(Unit)
            is ParsedResponse.KnownError -> Either.Left(CheckLoginError.Known(parsedResponse.knownError))
            is ParsedResponse.Failure -> Either.Left(CheckLoginError.Failure(parsedResponse.failure))
        }
    }

}