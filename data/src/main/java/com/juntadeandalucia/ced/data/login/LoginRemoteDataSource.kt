package com.juntadeandalucia.ced.data.login

import com.juntadeandalucia.ced.data.remote.NoContentResponse
import com.juntadeandalucia.ced.data.remote.ParsedResponse
import com.juntadeandalucia.ced.data.remote.ResponseParse
import com.juntadeandalucia.ced.data.remote.ResponseParse.Companion.REQUEST_OP_USER_BLOQUED
import com.juntadeandalucia.ced.data.remote.ResponseParse.Companion.REQUEST_OP_USER_ERROR
import com.juntadeandalucia.ced.domain.login.CheckLoginError
import com.juntadeandalucia.ced.domain.login.LoginError
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import kotlin.reflect.KClass
import kotlin.reflect.jvm.internal.impl.resolve.constants.KClassValue

class LoginRemoteDataSource(
    private val service: LoginRetrofit,
    private val responseParse: ResponseParse
) {
    suspend fun checkLogin(loginRequest: LoginRequest): ParsedResponse<LoginError, NoContentResponse> {
        val response: Response<ResponseBody> = service.checkLogin(
            loginRequest.username,
            loginRequest.password,
            loginRequest.version
        ).await()

        return responseParse.parseNoContentResponse(response, getError())
    }

    private fun getError(): Map<String, KClass<out LoginError>> = mapOf(
        REQUEST_OP_USER_BLOQUED to LoginError.UserBlocked::class,
        REQUEST_OP_USER_ERROR to LoginError.UserIncorrect::class
    )

}