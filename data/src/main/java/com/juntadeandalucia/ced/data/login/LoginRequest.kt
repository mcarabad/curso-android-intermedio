package com.juntadeandalucia.ced.data.login

data class LoginRequest(val username: String, val password: String, val version: String)