package com.juntadeandalucia.ced.newipasen.login

sealed class LoginViewTransition {
    data class onWelcome(val message: String) : LoginViewTransition()
}