package com.juntadeandalucia.ced.newipasen.login

import com.juntadeandalucia.ced.domain.RepositoryFailure
import com.juntadeandalucia.ced.domain.login.CheckLogin
import com.juntadeandalucia.ced.domain.login.CheckLoginError
import com.juntadeandalucia.ced.domain.login.LoginError
import com.juntadeandalucia.ced.domain.login.LoginInput
import com.juntadeandalucia.ced.newipasen.base.BaseViewModel

class LoginViewModel(val checkLogin: CheckLogin): BaseViewModel<LoginViewState, LoginViewTransition>() {
    private val state = viewState.value as? LoginViewState.Login ?: LoginViewState.Login()

    fun checkLogin(username: String, password: String) {
        state.loading = true
        viewState.value = state
        checkLogin(LoginInput(username, password, """{"version": "11.8.3.4"}""")) {
            state.loading = false

            it.fold(::handleError, ::handleSuccess)

            viewState.value = state
        }
    }

    private fun handleError(checkLoginError: CheckLoginError) {
        when (checkLoginError) {
            is CheckLoginError.Known -> when(checkLoginError.error) {
                is LoginError.UserIncorrect -> state.error = "Usuario incorrecto, por favor, revise los datos"
                is LoginError.UserBlocked -> state.error = "Usuario bloqueado"
            }
            is CheckLoginError.Failure -> when(checkLoginError.error) {
                is RepositoryFailure.NoInternet -> state.error = "No hay conexión a internet, inténtelo más tarde"
                is RepositoryFailure.Unknown -> state.error = "Ha ocurrido un error inesperado"
                is RepositoryFailure.Unauthorized -> state.error = "No puedes acceder a la aplicación"
                is RepositoryFailure.ServerError -> state.error = "Tenemos problemas técnicos, por favor, inténtelo más tarde"
            }
        }
    }

    private fun handleSuccess(unit: Unit) {
        viewTransition.value = LoginViewTransition.onWelcome("Bienvenido!")
    }


}