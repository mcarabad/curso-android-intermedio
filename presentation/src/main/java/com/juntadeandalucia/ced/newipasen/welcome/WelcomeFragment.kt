package com.juntadeandalucia.ced.newipasen.welcome

import com.juntadeandalucia.ced.newipasen.R
import com.juntadeandalucia.ced.newipasen.base.BaseFragment
import kotlinx.android.synthetic.main.welcome_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class WelcomeFragment: BaseFragment<WelcomeViewState, WelcomeViewTransition>() {
    override fun getLayout(): Int {
        return R.layout.welcome_fragment
    }

    override fun initViews() {
        initListeners()
    }

    override fun manageState(state: WelcomeViewState) {
        when (state) {
            is WelcomeViewState.Welcome -> {
                if (state.message.isNotEmpty()) tvWelcome.text = state.message
            }
        }
    }

    override fun manageTransition(transition: WelcomeViewTransition) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun initListeners() {

    }

    override val viewModel by viewModel<WelcomeViewModel>()


}