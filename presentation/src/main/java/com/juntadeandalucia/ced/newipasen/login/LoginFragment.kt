package com.juntadeandalucia.ced.newipasen.login

import android.view.View
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.juntadeandalucia.ced.newipasen.R
import com.juntadeandalucia.ced.newipasen.base.BaseFragment
import com.juntadeandalucia.ced.newipasen.operations.Login.LoginFragmentDirections
import kotlinx.android.synthetic.main.login_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment<LoginViewState, LoginViewTransition>() {
    override val viewModel by viewModel<LoginViewModel>()

    override fun getLayout(): Int {
        return R.layout.login_fragment
    }

    override fun initViews() {
        initListeners()
    }

    override fun manageState(state: LoginViewState) {
        when (state) {
            is LoginViewState.Login -> {
                if (state.loading) {
                    progress.visibility = View.VISIBLE
                } else {
                    progress.visibility = View.GONE
                }

                if (state.error.isNotEmpty()) Toast
                    .makeText(context, state.error, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun manageTransition(transition: LoginViewTransition) {
        when (transition) {
            is LoginViewTransition.onWelcome -> findNavController()
                .navigate(
                    LoginFragmentDirections
                        .actionLoginFragmentToWelcomeFragment(transition.message)
                )
        }
    }

    override fun initListeners() {
        btnLogin.setOnClickListener {
            val username = etUsername.text.toString()
            val password = etPassword.text.toString()
            if (username.isNotEmpty() && password.isNotEmpty()) {
                viewModel.checkLogin(username, password)
            } else {
                etUsername.error = "Por favor, rellene este campo"
                etPassword.error = "Por favor, rellene este campo"
            }
        }
    }

}