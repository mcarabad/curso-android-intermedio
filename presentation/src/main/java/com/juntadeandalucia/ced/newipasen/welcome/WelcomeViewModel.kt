package com.juntadeandalucia.ced.newipasen.welcome

import com.juntadeandalucia.ced.newipasen.base.BaseViewModel

class WelcomeViewModel: BaseViewModel<WelcomeViewState, WelcomeViewTransition>() {
    private val state = viewState.value as? WelcomeViewState.Welcome ?: WelcomeViewState.Welcome()
}