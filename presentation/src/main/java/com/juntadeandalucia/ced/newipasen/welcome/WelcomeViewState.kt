package com.juntadeandalucia.ced.newipasen.welcome

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

sealed class WelcomeViewState : Parcelable {

    @Parcelize
    data class Welcome(var message: String = ""): WelcomeViewState()
}