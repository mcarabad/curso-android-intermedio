package com.juntadeandalucia.ced.newipasen.login

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

sealed class LoginViewState: Parcelable {

    @Parcelize
    data class Login(var error: String = "", var logged: Boolean = false, var loading: Boolean = false) : LoginViewState()
}